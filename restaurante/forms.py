from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Usuario

class FormularioRegistro(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = Usuario
        fields = ('username','email','nombres','apellidos')

class FormularioEditar(UserChangeForm):
    class Meta:
        model = Usuario
        fields = UserChangeForm.Meta.fields