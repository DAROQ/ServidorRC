# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from rest_framework import viewsets
from .models import Usuario
from .serializers import UsuarioSerializer

from .forms import FormularioRegistro

# Create your views here.
class Registrar(generic.CreateView):
    form_class = FormularioRegistro
    success_url = reverse_lazy('login')
    template_name = 'signup.html'

class UsuarioViewSet(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    serializer_class = UsuarioSerializer