# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser
from django.db import models

class Usuario(AbstractUser):
    nombres = models.CharField(blank = True, max_length = 255)
    apellidos = models.CharField(blank = True, max_length = 255)
    
    def __str__(self):
        return self.email

class Ingrediente(models.Model):
	nombre_ingrediente = models.CharField(max_length=200)
	platofuerte = models.ForeignKey('PlatoFuerte', on_delete=models.CASCADE)
	bebida = models.ForeignKey('Bebida', on_delete=models.CASCADE)
	acompanamiento = models.ForeignKey('Acompanamiento', on_delete=models.CASCADE)

	"""docstring for ClassName"""
	def __str__(self):
		return self.nombre_ingrediente

class PlatoFuerte(models.Model):
	nombrePlato = models.CharField(blank = True, max_length =255)
	precioPlato = models.IntegerField(default=0)
	tipoPlato = models.CharField(blank = True, max_length = 255)
	pedido = models.ForeignKey('Pedido', on_delete=models.CASCADE, blank=True, null=True)

	"""docstring for ClassName"""
	def __str__(self):
		return self.nombrePlato

class Bebida(models.Model):	
	nombreBebida = models.CharField(blank = True, max_length = 255)
	precioBebida = models.IntegerField(default=0)
	tipoBebida = models.CharField(blank = True, max_length = 255)
	pedido = models.ForeignKey('Pedido', on_delete=models.CASCADE, blank=True, null=True)
	"""docstring for Bebida"""
	def __str__(self):
		return self.nombreBebida

class Acompanamiento(models.Model):
	nombreAcom = models.CharField(blank = True, max_length = 255)
	precioAcom = models.IntegerField(default=0)
	tipoAcom = models.CharField(blank = True, max_length = 255)
	pedido = models.ForeignKey('Pedido', on_delete=models.CASCADE, blank=True, null=True)

	"""docstring for ClassName"""
	def __str__(self):
		return self.nombreAcom

class Pedido(models.Model):
	nombre_Cliente = models.CharField(blank = True, max_length = 255)
	cedula_Cliente = models.IntegerField(default=0)
	direccion_Cliente =  models.CharField(blank = True, max_length = 255)
	telefono_Cliente = models.IntegerField(default=0)
	"""docstring for Pedido"""
	def __init__(self, arg):
		super(Pedido, self).__init__()
		self.arg = arg
		
		